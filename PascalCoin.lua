-- Inofficial PascalCoin Extension for MoneyMoney
--
-- Fetches balances of PASAs via the official Pascal Block Explorer API. As the
-- API is rather limited, a list of PASA must be provided; fetching all PASA for
-- a public key is not supported.
-- PascalCoin exchange rate is retrieved from the CoinGecko API.
--
-- All assets are returned as securities.
--
-- Username: List of PASA (space or comma-separated)
-- Password: ignored

-- MIT License
--
-- Copyright (C) 2022, Michael Maier.
-- All rights reserved.
--
-- Permission is hereby granted, free of charge, to any person obtaining a copy
-- of this software and associated documentation files (the "Software"), to deal
-- in the Software without restriction, including without limitation the rights
-- to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
-- copies of the Software, and to permit persons to whom the Software is
-- furnished to do so, subject to the following conditions:
--
-- The above copyright notice and this permission notice shall be included in
-- all copies or substantial portions of the Software.
--
-- THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
-- IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
-- FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
-- AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
-- LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
-- OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
-- SOFTWARE.

local MMAccountTypePortfolio = "AccountTypePortfolio"

local PasaServiceName = "PascalCoin (PASA)"
local NativeCurrency = "EUR"

local PASA
local CryptoMarket


WebBanking {
    version = 1.0,
    services = { PasaServiceName },
    description = "Fetch PASA balances from Pascal Block Explorer and list them as securities.",
    url = ""
}


--- Array of PASA objects extracted from username during session initialisation.
local sessionAccounts
local sessionConnection


--- Check authority for service.
function SupportsBank(protocol, bankCode)
    return protocol == ProtocolWebBanking and bankCode == PasaServiceName
end

--- Initialise session. As the only configuration is a list of PASA in the
--- username, extract all PASA.
function InitializeSession(protocol, bankCode, username, reserved, password)
    sessionConnection = Connection()
    sessionConnection.language = "en-US"

    sessionAccounts = {}

    -- Split PASA by matching all that is not a space or (semi)comma delimiter.
    for pasa in string.gmatch(username, "([^,;%s]+)") do
        table.insert(sessionAccounts, PASA(pasa))
    end

    if not #sessionAccounts then
        return "Please provide one or multiple PASA in the username field, separated by space or comma."
    end
end

function ListAccounts(knownAccounts)
    local account = {
        name = "PascalCoin",
        accountNumber = "PASA",
        currency = NativeCurrency,
        portfolio = true,
        type = MMAccountTypePortfolio
    }

    return { account }
end

function RefreshAccount (account, since)
    local securities = {}
    local currentExchangeRate = CryptoMarket:exchangeRateFor("PascalCoin", NativeCurrency)

    print(
        string.format(
            "Current %s to %s exchange rate is %f.",
            "PASC",
            NativeCurrency,
            currentExchangeRate
        )
    )

    for _, pasa in ipairs(sessionAccounts) do
        print(string.format("Fetching balance for %s.", pasa))

        local balance = pasa:balance()

        table.insert(securities, {
            name = tostring(pasa),
            currency = nil,
            market = CryptoMarket.name,
            quantity = balance,
            price = currentExchangeRate,
            prettyPrint = false,
            originalCurrencyAmount = balance,
            currencyOfOriginalAmount = "PASC"
        })
    end

    return { securities = securities }
end

function EndSession()
    sessionConnection:close()
    sessionConnection = nil
end


--- Crypto market interface.
CryptoMarket = {
    --- Name of market represented by this interface.
    name = "CoinGecko"
}

--- Fetches the market exchange rate for a crypto to fiat currency.
---@param cryptoCurrency string Crypto currency symbol to fetch exchange rate for.
---@param fiatCurrency string   Target fiat currency symbol to fetch exchange rate for.
---@return number   @Exchange rate for crypto to fiat currency.
function CryptoMarket:exchangeRateFor(cryptoCurrency, fiatCurrency)
    assert(type(cryptoCurrency) == "string", "Source must be a valid crypto currency symbol string.")
    assert(type(fiatCurrency) == "string", "Target must be a valid fiat currency symbol string.")

    cryptoCurrency = cryptoCurrency:lower()
    fiatCurrency = fiatCurrency:lower()

    local url = string.format(
        "https://api.coingecko.com/api/v3/simple/price?ids=%s&vs_currencies=%s",
        MM.urlencode(cryptoCurrency),
        MM.urlencode(fiatCurrency)
    )
    local response = sessionConnection:request("GET", url)
    local result = JSON(response):dictionary()

    assert(type(result) == "table", "Invalid response from CoinGecko API.")
    assert(type(result[cryptoCurrency]) == "table", "Failed to fetch exchange rate: No crypto currency entry in result set.")
    assert(result[cryptoCurrency][fiatCurrency], "Failed to fetch exchange rate: No fiat currency entry in result set.")

    return tonumber(result[cryptoCurrency][fiatCurrency])
end


--- Object template to represent an immutable PASA.
--- Construct with the account number as the only argument.
PASA = {
    --- PASA ID (without checksum).
    ---@type integer
    id = 0
}

--- Retrieves the PASA balance from the PascalCoin Explorer API.
---@return number   @PASC balance of PASA.
function PASA:balance()
    local url = string.format(
        "http://explorer.pascalcoin.org/api.php?account=%s",
        MM.urlencode(tostring(self))
    )
    local response = Connection():request("GET", url)
    local result = JSON(response):dictionary()

    assert(type(result) == "table", "Invalid response from Pascal Explorer API.")
    assert(result["balance"], "Failed to fetch PASA balance: Pascal Explorer API did not return a balance.")

    return tonumber(result["balance"])
end

--- Calculates the checksum for the PASA ID.
---@return integer
function PASA:checksum()
    -- https://github.com/PascalCoin/PascalCoin/blob/6a875320a9f370b67a93b26e300ba1e53d4dfe8e/src/core/UEPasa.pas#L386
    return ((self.id * 101) % 89) + 10
end

--- Returns a string representation of the PASA composed of the PASA ID and the
--- corresponding checksum. This is the default textual representation of a PASA
--- for the user.
---@return string   @PASA ID with checksum.
function PASA:__tostring()
    return string.format("%d-%02d", self.id, self:checksum())
end

do
    local function integerID(pasa)
        local numericPASA = pasa

        if(type(numericPASA) ~= "number") then
            assert(type(numericPASA) == "string", "PASA must be a number or string.")

            -- Remove optional checksum.
            numericPASA = numericPASA:gsub("(%d+)(-%d+)", "%1")
            numericPASA = tonumber(numericPASA)

            if type(numericPASA) ~= "number" then
                error(string.format("Invalid PASA identifier '%s'.", pasa))
            end
        end

        -- Ensure an integer is returned.
        return math.floor(numericPASA)
    end

    setmetatable(PASA, {
        __call = function (self, id)
            self.__index = self

            return setmetatable({
                id = integerID(id)
            }, self)
        end
    });
end